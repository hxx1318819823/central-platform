package com.central.cms.service.impl;

import com.central.cms.service.CpCmsTopicService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsTopic;
import com.central.cms.mybatis.mapper.CpCmsTopicMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsTopicServiceImpl extends BaseServiceImpl<CpCmsTopicMapper, CpCmsTopic> implements CpCmsTopicService {


}
