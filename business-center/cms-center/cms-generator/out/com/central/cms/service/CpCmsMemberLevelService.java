package com.central.cms.service;

import com.central.cms.commons.base.service.BaseService;
import com.central.cms.mybatis.model.CpCmsMemberLevel;

public interface CpCmsMemberLevelService extends BaseService<CpCmsMemberLevel>{

}
