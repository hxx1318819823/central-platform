package com.central.cms.service.impl;

import com.central.cms.service.CpCmsLinkageService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsLinkage;
import com.central.cms.mybatis.mapper.CpCmsLinkageMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsLinkageServiceImpl extends BaseServiceImpl<CpCmsLinkageMapper, CpCmsLinkage> implements CpCmsLinkageService {


}
