package com.central.cms.service.impl;

import com.central.cms.service.CpCmsContentService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsContent;
import com.central.cms.mybatis.mapper.CpCmsContentMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsContentServiceImpl extends BaseServiceImpl<CpCmsContentMapper, CpCmsContent> implements CpCmsContentService {


}
