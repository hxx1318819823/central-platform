package com.central.cms.service.impl;

import com.central.cms.service.CpCmsTagContentService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsTagContent;
import com.central.cms.mybatis.mapper.CpCmsTagContentMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsTagContentServiceImpl extends BaseServiceImpl<CpCmsTagContentMapper, CpCmsTagContent> implements CpCmsTagContentService {


}
