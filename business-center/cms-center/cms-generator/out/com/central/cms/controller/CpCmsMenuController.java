package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsMenu;
import com.central.cms.service.CpCmsMenuService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsMenuController extends BaseController {

    @Autowired
    private CpCmsMenuService cpcmsmenuService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsMenu cpcmsmenu = cpcmsmenuService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsmenu);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsmenuService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsMenu cpcmsmenu){
        cpcmsmenuService.insertSelective(cpcmsmenu);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsMenu cpcmsmenu){
        cpcmsmenuService.updateSelectiveById(cpcmsmenu);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsMenu queryBean){
        PageInfo<CpCmsMenu> page = cpcmsmenuService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
