package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsTagContent;
import com.central.cms.service.CpCmsTagContentService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsTagContentController extends BaseController {

    @Autowired
    private CpCmsTagContentService cpcmstagcontentService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsTagContent cpcmstagcontent = cpcmstagcontentService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmstagcontent);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmstagcontentService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsTagContent cpcmstagcontent){
        cpcmstagcontentService.insertSelective(cpcmstagcontent);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsTagContent cpcmstagcontent){
        cpcmstagcontentService.updateSelectiveById(cpcmstagcontent);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsTagContent queryBean){
        PageInfo<CpCmsTagContent> page = cpcmstagcontentService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
