package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsModelFiled;
import com.central.cms.service.CpCmsModelFiledService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsModelFiledController extends BaseController {

    @Autowired
    private CpCmsModelFiledService cpcmsmodelfiledService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsModelFiled cpcmsmodelfiled = cpcmsmodelfiledService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsmodelfiled);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsmodelfiledService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsModelFiled cpcmsmodelfiled){
        cpcmsmodelfiledService.insertSelective(cpcmsmodelfiled);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsModelFiled cpcmsmodelfiled){
        cpcmsmodelfiledService.updateSelectiveById(cpcmsmodelfiled);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsModelFiled queryBean){
        PageInfo<CpCmsModelFiled> page = cpcmsmodelfiledService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
