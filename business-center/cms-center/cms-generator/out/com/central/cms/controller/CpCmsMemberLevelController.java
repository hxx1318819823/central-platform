package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsMemberLevel;
import com.central.cms.service.CpCmsMemberLevelService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsMemberLevelController extends BaseController {

    @Autowired
    private CpCmsMemberLevelService cpcmsmemberlevelService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsMemberLevel cpcmsmemberlevel = cpcmsmemberlevelService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsmemberlevel);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsmemberlevelService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsMemberLevel cpcmsmemberlevel){
        cpcmsmemberlevelService.insertSelective(cpcmsmemberlevel);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsMemberLevel cpcmsmemberlevel){
        cpcmsmemberlevelService.updateSelectiveById(cpcmsmemberlevel);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsMemberLevel queryBean){
        PageInfo<CpCmsMemberLevel> page = cpcmsmemberlevelService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
